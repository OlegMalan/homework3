package Calendar.Weeks;
import Calendar.Helpers.Helper;
import java.util.Scanner;

public class Week {
    public  static void Calendar(){

        String[][] scedule = new String[7][2];
        Scanner sc = new Scanner(System.in);
        scedule[0][0] = "Sunday";
        scedule[0][1] = "Go home work";
        scedule[1][0] = "Monday";
        scedule[1][1] = "Go to courses; watch a film";
        scedule[2][0] = "Tuesday";
        scedule[2][1] = "Walk the dog";
        scedule[3][0] = "Wednesday";
        scedule[3][1] = "Visit the hospital";
        scedule[4][0] = "Thursday";
        scedule[4][1] = "Visit family";
        scedule[5][0] = "Friday";
        scedule[5][1] = "Go to town";
        scedule[6][0] = "Saturday";
        scedule[6][1] = "Walk in the park";


        for (String[] rowArray : scedule) {
         for (String value : rowArray) {// Виивід ігрового поля
                System.out.print(value + " ");
            }
            System.out.println();
        }

        while (true){
            Helper.outLine();
            System.out.print("Please, input the day of the week:");
            String nameWeek = sc.next();
            Helper.outLine();

            switch (nameWeek) {
                case "sunday", "Sunday", "SUNDAY", "sunday ", "Sunday ", "SUNDAY " ->
                        System.out.println("Your tasks for Sunday: " + scedule[0][1]);

                case "monday", "Monday", "MONDAY", "monday ", "Monday ", "MONDAY " ->
                        System.out.println("Your tasks for Monday: " + scedule[1][1]);

                case "tuesday", "Tuesday", "TUESDAY", "tuesday ", "Tuesday ", "TUESDAY " ->
                        System.out.println("Your tasks for Tuesday: " + scedule[2][1]);

                case "wednesday", "Wednesday", "WEDNESDAY", "wednesday ", "Wednesday ", "WEDNESDAY " ->
                        System.out.println("Your tasks for Wednesday: " + scedule[3][1]);

                case "thursday", "Thursday", "THURSDAY", "thursday ", "Thursday ", "THURSDAY " ->
                        System.out.println("Your tasks for Thursday: " + scedule[4][1]);

                case "friday", "Friday", "FRIDAY", "friday ", "Friday ", "FRIDAY " ->
                        System.out.println("Your tasks for Friday: " + scedule[5][1]);

                case "saturday", "Saturday", "SATURDAY", "saturday ", "Saturday ", "SATURDAY " ->
                        System.out.println("Your tasks for Saturday: " + scedule[6][1]);

                case "exit", "Exit", "EXIT", "exit ", "Exit ", "EXIT " -> {
                    System.out.println("before meeting");
                    Helper.outLine();
                    System.exit(1);
                }
                default -> System.out.println("I don't understand you, please try again.");
            }




        }
    }


}
